FIRMWARE_IMAGES := \
    abl \
    aop \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    engineering_cdt \
    featenabler \
    hyp \
    imagefv \
    keymaster \
    modem \
    multiimgoem \
    oplus_sec \
    oplusstanvbk \
    qupfw \
    qweslicstore \
    shrm \
    splash \
    tz \
    uefisecapp \
    xbl_config \
    xbl

AB_OTA_PARTITIONS += $(FIRMWARE_IMAGES)

